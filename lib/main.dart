import 'package:flutter/material.dart';
import 'package:midterm1_exam/screen/Login.dart';
import 'package:midterm1_exam/screen/home.dart';
import 'package:midterm1_exam/screen/timetable.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Home',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
      home:HomeScreen(),
      ),
    );
  }
}

