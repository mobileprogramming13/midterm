import 'package:flutter/material.dart';
import 'package:flutter_image_slider/carousel.dart';
import 'package:flutter_image_slider/indicator/Circle.dart';
import 'package:carousel_slider/carousel_slider.dart';


class AnnounceScreen extends StatefulWidget{
  @override
  _AnnounceScreen  createState() => _AnnounceScreen();
    
  }

  
  class _AnnounceScreen extends State<AnnounceScreen>{
 List<String> _imgList = [
    'assets/images/page.png',
    'assets/images/page1.jpg',
    'assets/images/page2.jpg',
    'assets/images/page3.jpg',
    
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('ประกาศ'),
          backgroundColor: Colors.grey,
        ),
        body: Center(
          child: Container(
            child: CarouselSlider(
              options: CarouselOptions(
                height: double.infinity,
                autoPlayAnimationDuration: Duration(milliseconds: 1000),
                aspectRatio: 1.873,
                viewportFraction: 1.0,
                autoPlay: true,
              ),
              items: _imgList
                  .map((item) => Image.asset(
                        item,
                        fit: BoxFit.fill,
                        width: double.infinity,
                      ))
                  .toList(),
            ),
          ),
        ),
      ),
      );
    
  }
  }

