import 'package:flutter/material.dart';
import 'package:midterm1_exam/screen/Login.dart';
import 'package:midterm1_exam/screen/calendar.dart';
import 'package:midterm1_exam/screen/profile.dart';

class TimetableScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ตารางเรียนนิสิต"),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TextButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return ProfileScreen();
                        }),
                      );
                },
                child: const Text('นนณี รอดมา',style: TextStyle(fontSize: 20),),
                ),
                const Text(
                'สถานะ กำลังศึกษา'),
                const Text(
                'คณะ	คณะวิทยาการสารสนเทศ'),
                const Text(
                'หลักสูตร	วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ วิทยาศาสตรบัณฑิต (วท.บ.)'),
                const Text(
                'อ. ที่ปรึกษา	ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม'),
                const Text(''),
            Image.asset("assets/images/timetable.png"),
            ],
              
            
          ),
        ),
      ),
    );
  }
}
